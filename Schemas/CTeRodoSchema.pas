unit CTeRodoSchema;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRodo = interface;
  IXMLRodo_occ = interface;
  IXMLRodo_occList = interface;
  IXMLRodo_occ_emiOcc = interface;

{ IXMLRodo }

  IXMLRodo = interface(IXMLNode)
    ['{C3040518-3576-42D1-B35C-F9F06E1FE166}']
    { Property Accessors }
    function Get_RNTRC: UnicodeString;
    function Get_Occ: IXMLRodo_occList;
    procedure Set_RNTRC(Value: UnicodeString);
    { Methods & Properties }
    property RNTRC: UnicodeString read Get_RNTRC write Set_RNTRC;
    property Occ: IXMLRodo_occList read Get_Occ;
  end;

{ IXMLRodo_occ }

  IXMLRodo_occ = interface(IXMLNode)
    ['{E8DBAD51-AE52-4984-86A5-E3991E92204C}']
    { Property Accessors }
    function Get_Serie: UnicodeString;
    function Get_NOcc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_EmiOcc: IXMLRodo_occ_emiOcc;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NOcc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
    { Methods & Properties }
    property Serie: UnicodeString read Get_Serie write Set_Serie;
    property NOcc: UnicodeString read Get_NOcc write Set_NOcc;
    property DEmi: UnicodeString read Get_DEmi write Set_DEmi;
    property EmiOcc: IXMLRodo_occ_emiOcc read Get_EmiOcc;
  end;

{ IXMLRodo_occList }

  IXMLRodo_occList = interface(IXMLNodeCollection)
    ['{2E4F025D-177F-44BD-B55E-2B369F3E5132}']
    { Methods & Properties }
    function Add: IXMLRodo_occ;
    function Insert(const Index: Integer): IXMLRodo_occ;

    function Get_Item(Index: Integer): IXMLRodo_occ;
    property Items[Index: Integer]: IXMLRodo_occ read Get_Item; default;
  end;

{ IXMLRodo_occ_emiOcc }

  IXMLRodo_occ_emiOcc = interface(IXMLNode)
    ['{3C375F80-5118-4D4C-8080-A55296F69894}']
    { Property Accessors }
    function Get_CNPJ: UnicodeString;
    function Get_CInt: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
    { Methods & Properties }
    property CNPJ: UnicodeString read Get_CNPJ write Set_CNPJ;
    property CInt: UnicodeString read Get_CInt write Set_CInt;
    property IE: UnicodeString read Get_IE write Set_IE;
    property UF: UnicodeString read Get_UF write Set_UF;
    property Fone: UnicodeString read Get_Fone write Set_Fone;
  end;

{ Forward Decls }

  TXMLRodo = class;
  TXMLRodo_occ = class;
  TXMLRodo_occList = class;
  TXMLRodo_occ_emiOcc = class;

{ TXMLRodo }

  TXMLRodo = class(TXMLNode, IXMLRodo)
  private
    FOcc: IXMLRodo_occList;
  protected
    { IXMLRodo }
    function Get_RNTRC: UnicodeString;
    function Get_Occ: IXMLRodo_occList;
    procedure Set_RNTRC(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_occ }

  TXMLRodo_occ = class(TXMLNode, IXMLRodo_occ)
  protected
    { IXMLRodo_occ }
    function Get_Serie: UnicodeString;
    function Get_NOcc: UnicodeString;
    function Get_DEmi: UnicodeString;
    function Get_EmiOcc: IXMLRodo_occ_emiOcc;
    procedure Set_Serie(Value: UnicodeString);
    procedure Set_NOcc(Value: UnicodeString);
    procedure Set_DEmi(Value: UnicodeString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLRodo_occList }

  TXMLRodo_occList = class(TXMLNodeCollection, IXMLRodo_occList)
  protected
    { IXMLRodo_occList }
    function Add: IXMLRodo_occ;
    function Insert(const Index: Integer): IXMLRodo_occ;

    function Get_Item(Index: Integer): IXMLRodo_occ;
  end;

{ TXMLRodo_occ_emiOcc }

  TXMLRodo_occ_emiOcc = class(TXMLNode, IXMLRodo_occ_emiOcc)
  protected
    { IXMLRodo_occ_emiOcc }
    function Get_CNPJ: UnicodeString;
    function Get_CInt: UnicodeString;
    function Get_IE: UnicodeString;
    function Get_UF: UnicodeString;
    function Get_Fone: UnicodeString;
    procedure Set_CNPJ(Value: UnicodeString);
    procedure Set_CInt(Value: UnicodeString);
    procedure Set_IE(Value: UnicodeString);
    procedure Set_UF(Value: UnicodeString);
    procedure Set_Fone(Value: UnicodeString);
  end;

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
function Loadrodo(const FileName: string): IXMLRodo;
function Newrodo: IXMLRodo;

const
  TargetNamespace = 'http://www.portalfiscal.inf.br/cte';
  TargetSignatureNamespace = 'http://www.w3.org/2000/09/xmldsig#';

implementation

{ Global Functions }

function Getrodo(Doc: IXMLDocument): IXMLRodo;
begin
  Result := Doc.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Loadrodo(const FileName: string): IXMLRodo;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

function Newrodo: IXMLRodo;
begin
  Result := NewXMLDocument.GetDocBinding('rodo', TXMLRodo, TargetNamespace) as IXMLRodo;
end;

{ TXMLRodo }

procedure TXMLRodo.AfterConstruction;
begin
  RegisterChildNode('occ', TXMLRodo_occ);
  FOcc := CreateCollection(TXMLRodo_occList, IXMLRodo_occ, 'occ') as IXMLRodo_occList;
  inherited;
end;

function TXMLRodo.Get_RNTRC: UnicodeString;
begin
  Result := ChildNodes['RNTRC'].Text;
end;

procedure TXMLRodo.Set_RNTRC(Value: UnicodeString);
begin
  ChildNodes['RNTRC'].NodeValue := Value;
end;

function TXMLRodo.Get_Occ: IXMLRodo_occList;
begin
  Result := FOcc;
end;

{ TXMLRodo_occ }

procedure TXMLRodo_occ.AfterConstruction;
begin
  RegisterChildNode('emiOcc', TXMLRodo_occ_emiOcc);
  inherited;
end;

function TXMLRodo_occ.Get_Serie: UnicodeString;
begin
  Result := ChildNodes['serie'].Text;
end;

procedure TXMLRodo_occ.Set_Serie(Value: UnicodeString);
begin
  ChildNodes['serie'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_NOcc: UnicodeString;
begin
  Result := ChildNodes['nOcc'].Text;
end;

procedure TXMLRodo_occ.Set_NOcc(Value: UnicodeString);
begin
  ChildNodes['nOcc'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_DEmi: UnicodeString;
begin
  Result := ChildNodes['dEmi'].Text;
end;

procedure TXMLRodo_occ.Set_DEmi(Value: UnicodeString);
begin
  ChildNodes['dEmi'].NodeValue := Value;
end;

function TXMLRodo_occ.Get_EmiOcc: IXMLRodo_occ_emiOcc;
begin
  Result := ChildNodes['emiOcc'] as IXMLRodo_occ_emiOcc;
end;

{ TXMLRodo_occList }

function TXMLRodo_occList.Add: IXMLRodo_occ;
begin
  Result := AddItem(-1) as IXMLRodo_occ;
end;

function TXMLRodo_occList.Insert(const Index: Integer): IXMLRodo_occ;
begin
  Result := AddItem(Index) as IXMLRodo_occ;
end;

function TXMLRodo_occList.Get_Item(Index: Integer): IXMLRodo_occ;
begin
  Result := List[Index] as IXMLRodo_occ;
end;

{ TXMLRodo_occ_emiOcc }

function TXMLRodo_occ_emiOcc.Get_CNPJ: UnicodeString;
begin
  Result := ChildNodes['CNPJ'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_CNPJ(Value: UnicodeString);
begin
  ChildNodes['CNPJ'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_CInt: UnicodeString;
begin
  Result := ChildNodes['cInt'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_CInt(Value: UnicodeString);
begin
  ChildNodes['cInt'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_IE: UnicodeString;
begin
  Result := ChildNodes['IE'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_IE(Value: UnicodeString);
begin
  ChildNodes['IE'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_UF: UnicodeString;
begin
  Result := ChildNodes['UF'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_UF(Value: UnicodeString);
begin
  ChildNodes['UF'].NodeValue := Value;
end;

function TXMLRodo_occ_emiOcc.Get_Fone: UnicodeString;
begin
  Result := ChildNodes['fone'].Text;
end;

procedure TXMLRodo_occ_emiOcc.Set_Fone(Value: UnicodeString);
begin
  ChildNodes['fone'].NodeValue := Value;
end;

end.

