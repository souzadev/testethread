unit CTeUtils;

interface

uses
  Xml.XMLIntf, Xml.XMLDoc, System.StrUtils, System.SysUtils, Constants,
  Connection, CTeSchema, CTeRodoSchema, Winapi.Windows, Data.DB, System.Classes;

function GenCTeXML(const IQryCTe: IQuery; Conn: IConnection): IXMLTCTe;

implementation

function GenCTeXML(const IQryCTe: IQuery; Conn: IConnection): IXMLTCTe;
var
  IQryDados: IQuery;
begin
  try
    Result := NewCTe;

    IQryDados := TQuery.New(Conn);

    // Informações do CT-e
    with Result.InfCte do
    begin
      Id     := 'CTe' + IQryCTe.Query.FieldByName('Id').AsString;
      Versao := CTE_VERSAO;

      //Identificação do CT-e
      with Ide do
      begin
        CUF     := IQryCTe.Query.FieldByName('cUF').AsString.Trim;
        CCT     := IQryCTe.Query.FieldByName('cCT').AsString.Trim;
        CFOP    := IQryCTe.Query.FieldByName('CFOP').AsString.Trim;
        NatOp   := IQryCTe.Query.FieldByName('natOp').AsString.Trim;
        Mod_    := IQryCTe.Query.FieldByName('mod').AsString.Trim;
        Serie   := IQryCTe.Query.FieldByName('serie').AsString.Trim;
        NCT     := IQryCTe.Query.FieldByName('nCT').AsString.Trim;
        DhEmi   := IQryCTe.Query.FieldByName('dhEmi').AsString.Trim;
        TpImp   := IQryCTe.Query.FieldByName('tpImp').AsString.Trim;
        TpEmis  := IQryCTe.Query.FieldByName('tpEmis').AsString.Trim;
        CDV     := IQryCTe.Query.FieldByName('cDV').AsString.Trim;
        TpAmb   := IQryCTe.Query.FieldByName('tpAmb').AsString.Trim;
        TpCTe   := IQryCTe.Query.FieldByName('tpCTe').AsString.Trim;
        ProcEmi := IQryCTe.Query.FieldByName('procEmi').AsString.Trim;
        VerProc := IQryCTe.Query.FieldByName('verProc').AsString.Trim;
        CMunEnv := IQryCTe.Query.FieldByName('cMunEmi').AsString.Trim;
        XMunEnv := IQryCTe.Query.FieldByName('xMunEmi').AsString.Trim;
        UFEnv   := IQryCTe.Query.FieldByName('UFEmi').AsString.Trim;
        Modal   := IQryCTe.Query.FieldByName('modal').AsString.Trim;
        TpServ  := IQryCTe.Query.FieldByName('tpServ').AsString.Trim;
        CMunIni := IQryCTe.Query.FieldByName('cMunIni').AsString.Trim;
        XMunIni := IQryCTe.Query.FieldByName('xMunIni').AsString.Trim;
        UFIni   := IQryCTe.Query.FieldByName('UFIni').AsString.Trim;
        CMunFim := IQryCTe.Query.FieldByName('cMunFim').AsString.Trim;
        XMunFim := IQryCTe.Query.FieldByName('xMunFim').AsString.Trim;
        UFFim   := IQryCTe.Query.FieldByName('UFFim').AsString.Trim;
        Retira  := IQryCTe.Query.FieldByName('retira').AsString.Trim;
        if IQryCTe.Query.FieldByName('xDetRetira').AsString.Trim <> '' then
          XDetRetira := IQryCTe.Query.FieldByName('xDetRetira').AsString.Trim;
        IndIEToma := IQryCTe.Query.FieldByName('ind_IE_Toma').AsString.Trim;
      end;

      //Emitente
      IQryDados.Open('SELECT * FROM EMI_CONHEC_CTE_EMITE WHERE ID_EMI_CONHEC_CTE = ' +
                     IQryCTe.Query.FieldByName('ID_EMI_CONHEC_CTE').AsString);
      try
        if not IQryDados.Query.IsEmpty then
        begin
          with Emit do
          begin
            if IQryDados.Query.FieldByName('CNPJ').AsString.Trim <> '' then
              CNPJ := IQryDados.Query.FieldByName('CNPJ').AsString.Trim;
            IE := IQryDados.Query.FieldByName('IE').AsString.Trim;
            XNome := IQryDados.Query.FieldByName('xNome').AsString.Trim;
            if IQryDados.Query.FieldByName('xFant').AsString.Trim <> '' then
              XFant := IQryDados.Query.FieldByName('xFant').AsString.Trim;
            with EnderEmit do
            begin
              XLgr := IQryDados.Query.FieldByName('xLgr').AsString.Trim;
              Nro  := IQryDados.Query.FieldByName('nro').AsString.Trim;
              if IQryDados.Query.FieldByName('xCpl').AsString.Trim <> '' then
                XCpl  := IQryDados.Query.FieldByName('xCpl').AsString.Trim;
              XBairro := IQryDados.Query.FieldByName('xBairro').AsString.Trim;
              CMun := IQryDados.Query.FieldByName('cMun').AsString.Trim;
              XMun := IQryDados.Query.FieldByName('xMun').AsString.Trim;
              CEP  := IQryDados.Query.FieldByName('CEP').AsString.Trim;
              UF   := IQryDados.Query.FieldByName('UF').AsString.Trim;
              if IQryDados.Query.FieldByName('fone').AsString.Trim.Length > 7 then
                Fone := IQryDados.Query.FieldByName('fone').AsString.Trim;
            end;
          end;
        end;
      finally
        IQryDados.Close;
      end;

      //Remetente
      IQryDados.Open('SELECT * FROM EMI_CONHEC_CTE_REMET WHERE ID_EMI_CONHEC_CTE = ' +
                     IQryCTe.Query.FieldByName('ID_EMI_CONHEC_CTE').AsString);
      try
        if not IQryDados.Query.IsEmpty then
        begin
          with Rem do
          begin
            if IQryDados.Query.FieldByName('CNPJ').AsString.Trim <> '' then
              CNPJ := IQryDados.Query.FieldByName('CNPJ').AsString.Trim;
            if IQryDados.Query.FieldByName('CPF').AsString.Trim <> '' then
              CPF := IQryDados.Query.FieldByName('CPF').AsString.Trim;
            if IQryDados.Query.FieldByName('IE').AsString.Trim <> '' then
              IE  := IQryDados.Query.FieldByName('IE').AsString.Trim;
            XNome := IQryDados.Query.FieldByName('xNome').AsString.Trim;
            if IQryDados.Query.FieldByName('xFant').AsString.Trim <> '' then
              XFant := IQryDados.Query.FieldByName('xFant').AsString.Trim;
            if IQryDados.Query.FieldByName('fone').AsString.Trim.Length > 7 then
              Fone := IQryDados.Query.FieldByName('fone').AsString.Trim;
            with EnderReme do
            begin
              XLgr    := IQryDados.Query.FieldByName('xLgr').AsString.Trim;
              Nro     := IQryDados.Query.FieldByName('nro').AsString.Trim;
              if IQryDados.Query.FieldByName('xCpl').AsString.Trim <> '' then
                XCpl := IQryDados.Query.FieldByName('xCpl').AsString.Trim;
              XBairro := IQryDados.Query.FieldByName('xBairro').AsString.Trim;
              CMun    := IQryDados.Query.FieldByName('cMun').AsString.Trim;
              XMun    := IQryDados.Query.FieldByName('xMun').AsString.Trim;
              CEP     := IQryDados.Query.FieldByName('CEP').AsString.Trim;
              UF      := IQryDados.Query.FieldByName('UF').AsString.Trim;
              if IQryDados.Query.FieldByName('cPais').AsString.Trim <> '' then
                CPais := IQryDados.Query.FieldByName('cPais').AsString.Trim;
              if IQryDados.Query.FieldByName('xPais').AsString.Trim <> '' then
                XPais := IQryDados.Query.FieldByName('xPais').AsString.Trim;
            end;
            if IQryDados.Query.FieldByName('email').AsString.Trim <> '' then
              Email := IQryDados.Query.FieldByName('email').AsString.Trim;
          end;
        end;
      finally
        IQryDados.Close;
      end;

      //Destinatario
      IQryDados.Open('SELECT * FROM EMI_CONHEC_CTE_DEST WHERE ID_EMI_CONHEC_CTE = ' +
                     IQryCTe.Query.FieldByName('ID_EMI_CONHEC_CTE').AsString);
      try
        if not IQryDados.Query.IsEmpty then
        begin
          with Dest do
          begin
            if IQryDados.Query.FieldByName('CNPJ').AsString.Trim <> '' then
              CNPJ := IQryDados.Query.FieldByName('CNPJ').AsString.Trim;
            if IQryDados.Query.FieldByName('CPF').AsString.Trim <> '' then
              CPF := IQryDados.Query.FieldByName('CPF').AsString.Trim;
            if IQryDados.Query.FieldByName('IE').AsString.Trim <> '' then
              IE  := IQryDados.Query.FieldByName('IE').AsString.Trim;
            XNome := IQryDados.Query.FieldByName('xNome').AsString.Trim;
            if IQryDados.Query.FieldByName('fone').AsString.Trim.Length > 7 then
              Fone := IQryDados.Query.FieldByName('fone').AsString.Trim;
            if IQryDados.Query.FieldByName('ISUF').AsString.Trim <> '' then
              ISUF  := IQryDados.Query.FieldByName('ISUF').AsString.Trim;
            with EnderDest do
            begin
              XLgr    := IQryDados.Query.FieldByName('xLgr').AsString.Trim;
              Nro     := IQryDados.Query.FieldByName('nro').AsString.Trim;
              if IQryDados.Query.FieldByName('xCpl').AsString.Trim <> '' then
                XCpl := IQryDados.Query.FieldByName('xCpl').AsString.Trim;
              XBairro := IQryDados.Query.FieldByName('xBairro').AsString.Trim;
              CMun    := IQryDados.Query.FieldByName('cMun').AsString.Trim;
              XMun    := IQryDados.Query.FieldByName('xMun').AsString.Trim;
              CEP     := IQryDados.Query.FieldByName('CEP').AsString.Trim;
              UF      := IQryDados.Query.FieldByName('UF').AsString.Trim;
              if IQryDados.Query.FieldByName('cPais').AsString.Trim <> '' then
                CPais := IQryDados.Query.FieldByName('cPais').AsString.Trim;
              if IQryDados.Query.FieldByName('xPais').AsString.Trim <> '' then
                XPais := IQryDados.Query.FieldByName('xPais').AsString.Trim;
            end;
            if IQryDados.Query.FieldByName('email').AsString.Trim <> '' then
              Email := IQryDados.Query.FieldByName('email').AsString.Trim;
          end;
        end;
      finally
        IQryDados.Close;
      end;
    end;
  except
    on E: Exception do
      raise Exception.Create('GenCTeXML: ' + E.Message);
  end;
end;

end.

