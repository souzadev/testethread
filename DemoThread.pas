unit DemoThread;

interface

uses
  System.Classes,
  System.SysUtils,
  Winapi.ActiveX,
  Connection,
  Log,
  Winapi.Windows,
  CTeUtils,
  CTeSchema;

type

{ TDemoThread }

  TDemoThread = class(TThread)
  private
    FLog: TLog;
  protected
    procedure Execute; override;
  public
    constructor Create(ACreateSuspended: Boolean; APriority: TThreadPriority); reintroduce;
    destructor Destroy; override;
  end;

implementation

{ TDemoThread }

constructor TDemoThread.Create(ACreateSuspended: Boolean; APriority: TThreadPriority);
begin
  inherited Create(ACreateSuspended);
  FreeOnTerminate := True;
  Priority        := APriority;
  FLog := TLog.Create;
end;

destructor TDemoThread.Destroy;
begin
  FLog.Save(ExtractFilePath(ParamStr(0)) + 'Logs\', 'Thread_' + GetCurrentThreadID.ToString + '.log');
  FreeAndNil(FLog);
  inherited;
end;

procedure TDemoThread.Execute;
var
  IQryCTe: IQuery;
  Conn: IConnection;
  CTe: IXMLTCTe;
begin
  inherited;

  if Terminated then
    Exit;

  FLog.LogInfo('In�cio da Thread');

  try
    CoInitialize(nil);

    //Cria a Conex�o com o Banco
    FLog.LogInfo('Cria a Conex�o');
    Conn := TConnection.New;

    try
      IQryCTe := TQuery.New(Conn).Open('SELECT FIRST 10' +
                                       '       IDE.ID_EMI_CONHEC_CTE, ' +
                                       '       IDE.ID_EMI_CONHECIMENTO, ' +
                                       '       IDE.CUF, ' +
                                       '       IDE.CCT, ' +
                                       '       IDE.CFOP, ' +
                                       '       IDE.NATOP, ' +
                                       '       IDE.FORPAG, ' +
                                       '       IDE.MOD, ' +
                                       '       IDE.SERIE, ' +
                                       '       IDE.NCT, ' +
                                       '       IDE.DHEMI, ' +
                                       '       IDE.TPIMP, ' +
                                       '       IDE.TPEMIS, ' +
                                       '       IDE.CDV, ' +
                                       '       IDE.TPAMB, ' +
                                       '       IDE.TPCTE, ' +
                                       '       IDE.PROCEMI, ' +
                                       '       IDE.VERPROC, ' +
                                       '       IDE.REFCTE, ' +
                                       '       IDE.CMUNEMI, ' +
                                       '       IDE.XMUNEMI, ' +
                                       '       IDE.UFEMI, ' +
                                       '       IDE.MODAL, ' +
                                       '       IDE.TPSERV, ' +
                                       '       IDE.CMUNINI, ' +
                                       '       IDE.XMUNINI, ' +
                                       '       IDE.UFINI, ' +
                                       '       IDE.CMUNFIM, ' +
                                       '       IDE.XMUNFIM, ' +
                                       '       IDE.UFFIM, ' +
                                       '       IDE.RETIRA, ' +
                                       '       IDE.XDETRETIRA, ' +
                                       '       IDE.ID_EMI_CONHEC_CTE_STATUS, ' +
                                       '       IDE.PROTOCOLO, ' +
                                       '       IDE.LOTE, ' +
                                       '       IDE.ID, ' +
                                       '       IDE.EMAIL_TOMADOR, ' +
                                       '       IDE.DATA_ENVIO, ' +
                                       '       IDE.DATA_HORA, ' +
                                       '       IDE.MENSAGEM, ' +
                                       '       IDE.TOMA, ' +
                                       '       IDE.IND_IE_TOMA, ' +
                                       '       IDE.VERSAO, ' +
                                       '       EMIT.CNPJ ' +
                                       'FROM EMI_CONHEC_CTE IDE ' +
                                       'INNER JOIN EMI_CONHEC_CTE_EMITE EMIT ' +
                                       'ON EMIT.ID_EMI_CONHEC_CTE = IDE.ID_EMI_CONHEC_CTE');

      while not IQryCTe.Query.Eof do
      begin
        FLog.LogInfo('Gerando CT-e ' + IQryCTe.Query.FieldByName('ID').AsString);
        CTe := GenCTeXML(IQryCTe, Conn);
        IQryCTe.Query.Next;
      end;

      FLog.LogInfo('Processo executado com sucesso');
    except
      on E: Exception do
        FLog.LogError(E.Message);
    end;

    FLog.LogInfo('Fim da Thread');
  finally
    CoUninitialize;
  end;
end;

end.
