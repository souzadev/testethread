unit uPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Connection, Vcl.StdCtrls,
  Vcl.ExtCtrls, DemoThread, ShellAPI, Vcl.Samples.Spin;

type
  TfrmPrincipal = class(TForm)
    btnIniciar: TButton;
    tmThread: TTimer;
    lblThreadsRunning: TLabel;
    btnParar: TButton;
    lblThreadsCount: TLabel;
    spnQtdeThreads: TSpinEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnIniciarClick(Sender: TObject);
    procedure tmThreadTimer(Sender: TObject);
    procedure btnPararClick(Sender: TObject);
  private
    { Private declarations }
    ThreadsRunning, ThreadsCount: Integer;
    procedure ThreadTerminated(Sender: TObject);
    procedure AtualizaQtdeThreads;
    procedure DeleteDirectory(const DirName: string);
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

procedure TfrmPrincipal.AtualizaQtdeThreads;
begin
  lblThreadsRunning.Caption := IntToStr(ThreadsRunning) + ' Thread(s) em Execu��o';
  lblThreadsCount.Caption := IntToStr(ThreadsCount) + ' Thread(s) criadas';
end;

procedure TfrmPrincipal.btnIniciarClick(Sender: TObject);
begin
  if DirectoryExists(ExtractFilePath(ParamStr(0)) + 'Logs\') then
    DeleteDirectory(ExtractFilePath(ParamStr(0)) + 'Logs\');

  tmThread.Enabled := True;
  btnIniciar.Enabled := False;
  btnParar.Enabled := True;
  ThreadsCount := 0;
end;

procedure TfrmPrincipal.btnPararClick(Sender: TObject);
begin
  tmThread.Enabled := False;
  btnIniciar.Enabled := True;
  btnParar.Enabled := False;
end;

procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  //Cria o Pool de Conex�es
  TConnection.ConfiguraConexao;
end;

procedure TfrmPrincipal.ThreadTerminated(Sender: TObject);
begin
  Dec(ThreadsRunning);
  AtualizaQtdeThreads;
end;

procedure TfrmPrincipal.tmThreadTimer(Sender: TObject);
var
  Conn: IConnection;
  I: Integer;
begin
  if ThreadsRunning >= spnQtdeThreads.Value then
    Exit;

  //Cria a Thread
  with TDemoThread.Create(False, tpLower) do
    OnTerminate := ThreadTerminated;

  //Incrementa o n�mero de Threads ativas
  Inc(ThreadsRunning);
  Inc(ThreadsCount);
  AtualizaQtdeThreads;
end;

procedure TfrmPrincipal.DeleteDirectory(const DirName: string);
var
  FileOp: TSHFileOpStruct;
begin
  FillChar(FileOp, SizeOf(FileOp), 0);
  FileOp.wFunc := FO_DELETE;
  FileOp.pFrom := PChar(DirName+#0);
  FileOp.fFlags := FOF_SILENT or FOF_NOERRORUI or FOF_NOCONFIRMATION;
  SHFileOperation(FileOp);
end;

end.
