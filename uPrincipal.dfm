object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Teste Thread'
  ClientHeight = 275
  ClientWidth = 524
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblThreadsRunning: TLabel
    Left = 24
    Top = 112
    Width = 281
    Height = 33
    Caption = '0 Threads em execu'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblThreadsCount: TLabel
    Left = 24
    Top = 151
    Width = 210
    Height = 33
    Caption = '0 Threads criadas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 24
    Top = 27
    Width = 81
    Height = 13
    Caption = 'Qtde de Threads'
  end
  object btnIniciar: TButton
    Left = 24
    Top = 72
    Width = 97
    Height = 25
    Caption = 'Iniciar'
    TabOrder = 0
    OnClick = btnIniciarClick
  end
  object btnParar: TButton
    Left = 127
    Top = 72
    Width = 97
    Height = 25
    Caption = 'Parar'
    Enabled = False
    TabOrder = 1
    OnClick = btnPararClick
  end
  object spnQtdeThreads: TSpinEdit
    Left = 113
    Top = 18
    Width = 72
    Height = 22
    MaxValue = 10
    MinValue = 1
    TabOrder = 2
    Value = 10
  end
  object tmThread: TTimer
    Enabled = False
    OnTimer = tmThreadTimer
    Left = 40
    Top = 184
  end
end
