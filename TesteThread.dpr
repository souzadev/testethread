program TesteThread;

uses
  {$IFDEF DEBUG}
  FastMM4,
  {$ENDIF }
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {frmPrincipal},
  Connection in 'Connection.pas',
  Constants in 'Constants.pas',
  DemoThread in 'DemoThread.pas',
  CTeSchema in 'Schemas\CTeSchema.pas',
  CTeRodoSchema in 'Schemas\CTeRodoSchema.pas',
  CTeSignatureSchema in 'Schemas\CTeSignatureSchema.pas',
  CTeUtils in 'CTeUtils.pas',
  Log in 'Log.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := DebugHook <> 0;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
end.
