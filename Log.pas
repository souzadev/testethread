unit Log;

interface

uses
  System.SysUtils, System.StrUtils, System.Classes;

type
  TLog = class
  private
    FLog: TStringBuilder;
    function FormatLog(Log: string): string;
    procedure TextToFile(const AText: Widestring; FileName: string; Encoding: TEncoding);
  public
    constructor Create;
    destructor Destroy; override;
    procedure LogInfo(Log: string);
    procedure LogError(Log: string);
    procedure Add(Log: string);
    function Get: string;
    function GetLength: Integer;
    procedure Save(Folder, FileName: string);
    procedure AppendFile(Folder, FileName, Log: string);
  end;

implementation

{ TLog }

procedure TLog.Add(Log: string);
begin
  FLog.AppendLine(FormatLog(Log));
end;

procedure TLog.LogError(Log: string);
begin
  Add('ERROR: ' + Log);
end;

procedure TLog.LogInfo(Log: string);
begin
  Add('INFO: ' + Log);
end;

procedure TLog.AppendFile(Folder, FileName, Log: string);
var
  FileLog: TextFile;
begin
  FileName := Folder + FileName;
  Log := FormatLog(Log);

  AssignFile(FileLog, FileName);

  if FileExists(FileName) then
    Append(FileLog)
  else
    Rewrite(FileLog);

  Writeln(FileLog, Log);
  CloseFile(FileLog);
end;

constructor TLog.Create;
begin
  inherited Create;
  FLog := TStringBuilder.Create;
end;

destructor TLog.Destroy;
begin
  FreeAndNil(FLog);
  inherited Destroy;
end;

function TLog.Get: string;
begin
  if FLog.ToString.Trim.Length > 0 then
    Result := FLog.ToString
  else
    Result := '';
end;

function TLog.GetLength: Integer;
begin
  Result := FLog.ToString.Trim.Length;
end;

procedure TLog.Save(Folder, FileName: string);
begin
  if GetLength > 0 then
  begin
    if not DirectoryExists(Folder) then
      ForceDirectories(Folder);

    FileName := Folder + FileName;
    TextToFile(Get, FileName, TEncoding.ANSI);
  end;
end;

function TLog.FormatLog(Log: string): string;
begin
  Result := FormatDateTime('dd/mm/yyyy hh:nn:ss', Now) + ': ' + Log;
end;

procedure TLog.TextToFile(const AText: Widestring; FileName: string; Encoding: TEncoding);
var
  FFile: TstringList;
begin
  FFile := TstringList.Create;
  try
    FFile.Text := AText;
    FFile.SaveToFile(FileName, Encoding);
  finally
    FreeAndNil(FFile);
  end;
end;

end.
