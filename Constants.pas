unit Constants;

interface

uses
  System.Types;

const

  //Connection Database
  DB_DRIVER_ID: string = 'FB';
  DB_DATABASE: string = 'localhost:C:\Users\Marcelo\Desktop\TesteThread Firebird\Database\CTE.FDB';
  DB_USERNAME: string = 'SYSDBA';
  DB_PASSWORD: string = '172157';
  DB_POOLED: Boolean = True;
  DB_CHARSET: string = 'ISO8859_1';
  DB_POOL_NAME: string = 'CTE_SERVICE_POOL';
  DB_POOL_CLEANUP_TIMEOUT: Integer = 30000;
  DB_POOL_EXPIRE_TIMEOUT: Integer = 90000;
  DB_POOL_SIZE: Integer = 50;

  //Namespaces CT-e
  NAMESPACE_CTE: string = 'http://www.portalfiscal.inf.br/cte';
  NAMESPACE_CTE_RECEPCAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcao';
  NAMESPACE_CTE_RETRECEPCAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRetRecepcao';
  NAMESPACE_CTE_CANCELAMENTO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteCancelamento';
  NAMESPACE_CTE_INUTILIZACAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteInutilizacao';
  NAMESPACE_CTE_RECEPCAO_EVENTO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento';
  NAMESPACE_CTE_CONSULTA: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta';
  NAMESPACE_CTE_STATUS_SERVICO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteStatusServico';

  //SoapActions CT-e
  SOAPACTION_CTE_RECEPCAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcao/cteRecepcaoLote';
  SOAPACTION_CTE_RETRECEPCAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRetRecepcao/cteRetRecepcao';
  SOAPACTION_CTE_CANCELAMENTO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteCancelamento/cteCancelamentoCT';
  SOAPACTION_CTE_INUTILIZACAO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteInutilizacao/cteInutilizacaoCT';
  SOAPACTION_CTE_RECEPCAO_EVENTO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteRecepcaoEvento/cteRecepcaoEvento';
  SOAPACTION_CTE_CONSULTA: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteConsulta/cteConsultaCT';
  SOAPACTION_CTE_STATUS_SERVICO: string = 'http://www.portalfiscal.inf.br/cte/wsdl/CteStatusServico/cteStatusServicoCT';

  //SopaAction CL-e
  SOAPACTION_CLE_CADASTRO: string = 'http://www.portalfiscal.inf.br/nfe/wsdl/CleCadastro/cleCadastroLote';

  //Evento CT-e
  CTE_EVENTO_CANCELAMENTO: string = '110111';
  CTE_EVENTO_CARTA_CORRECAO: string = '110110';

  CTE_VERSAO: string = '3.00';
  AMBIENTE: Integer = 2;

  //C�digo de Status CT-e
  CSTAT_AUTORIZADO: string = '100';
  CSTAT_CANCELADO: string = '101';
  CSTAT_INUTILIZADO: string = '102';
  CSTAT_LOTE_RECEBIDO: string = '103';
  CSTAT_LOTE_PROCESSADO: string = '104';
  CSTAT_LOTE_EM_PROCESSAMENTO: string = '105';
  CSTAT_STATUS_SERVICO_OK: string = '107';
  CSTAT_CORRIGIDO: string = '135';
  CSTAT_EVENTO_CANCELAMENTO: string = '135';
  CSTAT_PARALISADO_MONENTANEAMENTE: string = '108';
  CSTAT_PARALISADO_SEM_PREVISAO: string = '109';


  UF_CODIGO_ARRAY: TIntegerDynArray =
    [12,27,16,13,29,23,53,32,52,21,51,50,31,15,25,41,26,22,33,24,43,11,14,42,35,28,17];

  UF_SIGLA_ARRAY: TStringDynArray =
    ['AC','AL','AP','AM','BA','CE','DF','ES','GO','MA','MT','MS','MG','PA',
     'PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO'];

  UF_DESCR_ARRAY: TStringDynArray =
    ['Acre','Alagoas','Amap�','Amazonas','Bahia','Cear�','Distrito Federal','Esp�rito Santo',
     'Goi�s','Maranh�o','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Par�','Para�ba',
     'Paran�','Pernambuco','Piau�','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul',
     'Rond�nia','Roraima','Santa Catarina','S�o Paulo','Sergipe','Tocantins'];

  TP_AMB_ARRAY: TIntegerDynArray = [1, 2];
  TP_AMB_DESCR_ARRAY: TStringDynArray = ['Produ��o', 'Homologa��o'];

implementation

end.
