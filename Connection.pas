unit Connection;

interface

uses
  System.SysUtils, System.Types, Constants, System.Classes, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Intf, FireDAC.Stan.Option, System.StrUtils,
  FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Stan.Param, FireDAC.UI.Intf, FireDAC.Phys.FB,
  FireDAC.Phys.OracleDef, FireDAC.Phys.Oracle, FireDAC.Comp.UI;

type
  IConnection = interface
    ['{EFE92FD4-E83C-4B42-A4B9-4ED8E1F89CF5}']
    function Connection : TCustomConnection;
    procedure BeginTrans;
    procedure Commit;
    procedure RollBack;
  end;

  IQuery = interface
    ['{9D8E55AB-22E0-440D-A77F-820B8B7F6D5C}']
    function Query: TDataSet;
    function ExecSQL(ASQL: string): Boolean;
    function ExecSQLScalar(ASQL: string): Variant;
    function Open(ASQL: string): IQuery;
    procedure Close;
  end;

  IStoredProc = interface
    ['{6F002272-D446-4D39-84C4-2B3E6BA744B0}']
    function Prepare(AProcName: string): IStoredProc;
    function Execute: IStoredProc;
    function AddParameter(AName: string; AValue: Variant): IStoredProc; overload;
    function AddParameter(AName: string; AValue: TStream): IStoredProc; overload;
  end;

  TConnection = class(TInterfacedObject, IConnection)
  private
    FConnection: TFDConnection;
  public
    constructor Create;
    destructor Destroy; override;
    class function New: IConnection;
    class procedure ConfiguraConexao;
    function Connection : TCustomConnection;
    procedure BeginTrans;
    procedure Commit;
    procedure RollBack;
  end;

  TQuery = class(TInterfacedObject, IQuery)
  private
    FQuery : TFDQuery;
    FConnection : IConnection;
  public
    constructor Create(Connection: IConnection);
    destructor Destroy; override;
    class function New(Connection: IConnection): IQuery;
    function Query: TDataSet;
    function ExecSQL(ASQL: string): Boolean;
    function ExecSQLScalar(ASQL: string): Variant;
    function Open(ASQL: string): IQuery;
    procedure Close;
  end;

  TStoredProc = class(TInterfacedObject, IStoredProc)
  private
    FStoredProc: TFDStoredProc;
    FConnection : IConnection;
  public
    constructor Create(Connection: IConnection);
    destructor Destroy; override;
    class function New(Connection: IConnection): IStoredProc;
    function Prepare(AProcName: string): IStoredProc;
    function Execute: IStoredProc;
    function AddParameter(AName: string; AValue: Variant): IStoredProc; overload;
    function AddParameter(AName: string; AValue: TStream): IStoredProc; overload;
  end;

implementation

{ TConnection }

class procedure TConnection.ConfiguraConexao;
var
  Params: TStrings;
begin
  Params := TStringList.Create;
  try
    Params.Add('DriverID=' + DB_DRIVER_ID);
    Params.Add('Database=' + DB_DATABASE);
    Params.Add('User_Name=' + DB_USERNAME);
    Params.Add('Password=' + DB_PASSWORD);
    Params.Add('Pooled=' + IfThen(DB_POOLED, 'True', 'False'));
    Params.Add('CharacterSet=' + DB_CHARSET);
    Params.Add('POOL_CleanupTimeout=' + DB_POOL_CLEANUP_TIMEOUT.ToString);
    Params.Add('POOL_ExpireTimeout=' + DB_POOL_EXPIRE_TIMEOUT.ToString);
    Params.Add('POOL_MaximumItems=' + DB_POOL_SIZE.ToString);

    if FDManager.IsConnectionDef(DB_POOL_NAME) then
      FDManager.DeleteConnectionDef(DB_POOL_NAME);

    FDManager.AddConnectionDef(DB_POOL_NAME, DB_DRIVER_ID, Params);
    FDManager.Open;
  finally
    FreeAndNil(Params);
  end;
end;

function TConnection.Connection: TCustomConnection;
begin
  Result := FConnection;
end;

procedure TConnection.BeginTrans;
begin
  if not FConnection.InTransaction then
    FConnection.StartTransaction;
end;

procedure TConnection.Commit;
begin
  FConnection.Commit;
end;

constructor TConnection.Create;
begin
  inherited Create;
  try
    FConnection := TFDConnection.Create(nil);
    FConnection.LoginPrompt := False;
    FConnection.ConnectionDefName := DB_POOL_NAME;
    FConnection.ResourceOptions.CmdExecTimeout := 30000;
    FConnection.Connected := True;
  except
    on E: Exception do
      raise Exception.Create('Erro ao Conectar o Banco de Dados: ' + E.Message);
  end;
end;

destructor TConnection.Destroy;
begin
  FreeAndNil(FConnection);
  inherited Destroy;
end;

class function TConnection.New: IConnection;
begin
  Result := Self.Create;
end;

procedure TConnection.RollBack;
begin
  if FConnection.InTransaction then
    FConnection.Rollback;
end;

{ TQuery }

procedure TQuery.Close;
begin
  FQuery.Close;
end;

constructor TQuery.Create(Connection: IConnection);
begin
  FConnection := Connection;
  FQuery := TFDQuery.Create(nil);
  FQuery.FetchOptions.RecordCountMode := TFDRecordCountMode.cmTotal;
  FQuery.Connection := TFDConnection(FConnection.Connection);
end;

destructor TQuery.Destroy;
begin
  FreeAndNil(FQuery);
  inherited;
end;

function TQuery.ExecSQLScalar(ASQL: string): Variant;
begin
  try
    Result := TFDConnection(FConnection.Connection).ExecSQLScalar(ASQL);
  except
    on E: Exception do
      raise Exception.Create('Erro ao Executar SQL Escalar: ' + E.Message);
  end;
end;

function TQuery.Open(ASQL: string): IQuery;
begin
  try
    Result := Self;

    if not Assigned(FQuery) then
      raise Exception.Create('FQuery � igual a Nil');

    FQuery.Open(ASQL);
  except
    on E: Exception do
      raise Exception.Create('Erro ao Abrir Query: ' + E.Message);
  end;
end;

function TQuery.ExecSQL(ASQL: string): Boolean;
begin
  try
    Result := FQuery.ExecSQL(ASQL) > 0;
  except
    on E: Exception do
      raise Exception.Create('Erro ao Executar SQL: ' + E.Message);
  end;
end;

class function TQuery.New(Connection: IConnection): IQuery;
begin
  Result := Self.Create(Connection);
end;

function TQuery.Query: TDataSet;
begin
  Result := FQuery;
end;

{ TStoredProc }

function TStoredProc.AddParameter(AName: string; AValue: TStream): IStoredProc;
begin
  try
    Result := Self;
    FStoredProc.Params.ParamByName(AName).LoadFromStream(AValue, ftOraBlob);
  except
    on E: Exception do
      raise Exception.Create('Erro ao Adicionar Par�metro: ' + E.Message);
  end;
end;

function TStoredProc.AddParameter(AName: string; AValue: Variant): IStoredProc;
begin
  try
    Result := Self;
    FStoredProc.Params.ParamByName(AName).Value := AValue;
  except
    on E: Exception do
      raise Exception.Create('Erro ao Adicionar Par�metro: ' + E.Message);
  end;
end;

constructor TStoredProc.Create(Connection: IConnection);
begin
  FConnection := Connection;
  FStoredProc := TFDStoredProc.Create(nil);
  FStoredProc.Connection := TFDConnection(FConnection.Connection);
end;

destructor TStoredProc.Destroy;
begin
  FreeAndNil(FStoredProc);
  inherited;
end;

function TStoredProc.Execute: IStoredProc;
begin
  try
    Result := Self;
    FStoredProc.ExecProc;
  except
    on E: Exception do
      raise Exception.Create('Erro ao Executar Stored Procedure: ' + E.Message);
  end;
end;

class function TStoredProc.New(Connection: IConnection): IStoredProc;
begin
  Result := Self.Create(Connection);
end;

function TStoredProc.Prepare(AProcName: string): IStoredProc;
begin
  try
    Result := Self;
    FStoredProc.StoredProcName := AProcName;
    FStoredProc.Prepare;
  except
    on E: Exception do
      raise Exception.Create('Erro ao Preparar Stored Procedure: ' + E.Message);
  end;
end;

end.
